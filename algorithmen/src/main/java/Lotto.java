import java.util.ArrayList;

public class Lotto {

    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        for (int i = 0; i < 6; i++) {
            int randomNum = (int) (Math.random() * ((45 - 1) + 1)) + 1;
            // If the random number does not already exist in the array num will be added, else another loop will be added
            if (!numbers.contains(randomNum)) {
                numbers.add(randomNum);
            } else {
                i--;
            }
        }
        printArray(numbers);
    }

    public static void printArray(ArrayList<Integer> numbers) {
        for (int num : numbers) {
            System.out.println(num);
        }
    }
}
