import java.util.ArrayList;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
//        System.out.println("RANDOM:" + System.lineSeparator());
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            System.out.println(random.nextInt());
        }
        System.out.println();
//        System.out.println("RANDOM SEED:" + System.lineSeparator());
        Random randomSeed = new Random(1000);
        for (int i = 0; i < 10; i++) {
            System.out.println(randomSeed.nextInt());
        }
    }
}
