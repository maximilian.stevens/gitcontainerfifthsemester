import java.util.Scanner;

public class SortString {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Stoppen sie das Programm indem sie 'stop' eingeben");
        while (true) {
            System.out.println("Geben Sie ein Wort ein");
            String input = scan.nextLine().toUpperCase();
            if (input.equals("STOP")) {
                break;
            }
            char[] chars = input.toCharArray();
            sortInput(chars);
            printArray(chars);
        }
    }

    public static void sortInput(char[] chars) {
        for (int i = 0; i < chars.length / 2; i++) {
            char tmp = chars[i];
            chars[i] = chars[chars.length - i - 1];
            chars[chars.length - i - 1] = tmp;
        }
    }

    public static void printArray(char[] chars) {
        for (char c : chars) {
            System.out.print(c);
        }
        System.out.println();
    }
}
