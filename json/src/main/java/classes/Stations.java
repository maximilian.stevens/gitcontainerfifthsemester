
package classes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Stations {

    private List<From> from = null;
    private List<To> to = null;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public List<From> getFrom() {
        return from;
    }

    public void setFrom(List<From> from) {
        this.from = from;
    }

    @Override
    public String toString() {
        return "Stations{" +
                "from=" + from +
                ", to=" + to +
                ", additionalProperties=" + additionalProperties +
                '}';
    }

    public List<To> getTo() {
        return to;
    }

    public void setTo(List<To> to) {
        this.to = to;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
