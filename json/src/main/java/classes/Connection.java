
package classes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Connection {

    private From from;
    private To to;
    private String duration;
    private Integer transfers;
    private Object service;
    private List<String> products = null;
    private Object capacity1st;
    private Object capacity2nd;
    private List<Section> sections = null;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public From getFrom() {
        return from;
    }

    public void setFrom(From from) {
        this.from = from;
    }

    public To getTo() {
        return to;
    }

    public void setTo(To to) {
        this.to = to;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getTransfers() {
        return transfers;
    }

    public void setTransfers(Integer transfers) {
        this.transfers = transfers;
    }

    public Object getService() {
        return service;
    }

    @Override
    public String toString() {
        return "Connection{" +
                "from=" + from +
                ", to=" + to +
                ", duration='" + duration + '\'' +
                ", transfers=" + transfers +
                ", service=" + service +
                ", products=" + products +
                ", capacity1st=" + capacity1st +
                ", capacity2nd=" + capacity2nd +
                ", sections=" + sections +
                ", additionalProperties=" + additionalProperties +
                '}';
    }

    public void setService(Object service) {
        this.service = service;
    }

    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {
        this.products = products;
    }

    public Object getCapacity1st() {
        return capacity1st;
    }

    public void setCapacity1st(Object capacity1st) {
        this.capacity1st = capacity1st;
    }

    public Object getCapacity2nd() {
        return capacity2nd;
    }

    public void setCapacity2nd(Object capacity2nd) {
        this.capacity2nd = capacity2nd;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
