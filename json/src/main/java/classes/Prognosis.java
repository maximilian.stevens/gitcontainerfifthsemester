
package classes;

import java.util.HashMap;
import java.util.Map;

public class Prognosis {

    private Object platform;
    private Object arrival;
    private Object departure;
    private Object capacity1st;
    private Object capacity2nd;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Object getPlatform() {
        return platform;
    }

    @Override
    public String toString() {
        return "Prognosis{" +
                "platform=" + platform +
                ", arrival=" + arrival +
                ", departure=" + departure +
                ", capacity1st=" + capacity1st +
                ", capacity2nd=" + capacity2nd +
                ", additionalProperties=" + additionalProperties +
                '}';
    }

    public void setPlatform(Object platform) {
        this.platform = platform;
    }

    public Object getArrival() {
        return arrival;
    }

    public void setArrival(Object arrival) {
        this.arrival = arrival;
    }

    public Object getDeparture() {
        return departure;
    }

    public void setDeparture(Object departure) {
        this.departure = departure;
    }

    public Object getCapacity1st() {
        return capacity1st;
    }

    public void setCapacity1st(Object capacity1st) {
        this.capacity1st = capacity1st;
    }

    public Object getCapacity2nd() {
        return capacity2nd;
    }

    public void setCapacity2nd(Object capacity2nd) {
        this.capacity2nd = capacity2nd;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
