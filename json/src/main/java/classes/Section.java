
package classes;

import java.util.HashMap;
import java.util.Map;

public class Section {

    private Journey journey;
    private Object walk;
    private Departure departure;
    private Arrival arrival;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @Override
    public String toString() {
        return "Section{" +
                "journey=" + journey +
                ", walk=" + walk +
                ", departure=" + departure +
                ", arrival=" + arrival +
                ", additionalProperties=" + additionalProperties +
                '}';
    }

    public Journey getJourney() {
        return journey;
    }

    public void setJourney(Journey journey) {
        this.journey = journey;
    }

    public Object getWalk() {
        return walk;
    }

    public void setWalk(Object walk) {
        this.walk = walk;
    }

    public Departure getDeparture() {
        return departure;
    }

    public void setDeparture(Departure departure) {
        this.departure = departure;
    }

    public Arrival getArrival() {
        return arrival;
    }

    public void setArrival(Arrival arrival) {
        this.arrival = arrival;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
