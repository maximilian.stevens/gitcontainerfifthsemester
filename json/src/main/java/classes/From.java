
package classes;

import java.util.HashMap;
import java.util.Map;

public class From {

    @Override
    public String toString() {
        return "From{" +
                "station=" + station +
                ", arrival=" + arrival +
                ", arrivalTimestamp=" + arrivalTimestamp +
                ", departure='" + departure + '\'' +
                ", departureTimestamp=" + departureTimestamp +
                ", delay=" + delay +
                ", platform='" + platform + '\'' +
                ", prognosis=" + prognosis +
                ", realtimeAvailability=" + realtimeAvailability +
                ", location=" + location +
                ", additionalProperties=" + additionalProperties +
                '}';
    }

    private Station station;
    private Object arrival;
    private Object arrivalTimestamp;
    private String departure;
    private Integer departureTimestamp;
    private Object delay;
    private String platform;
    private Prognosis prognosis;
    private Object realtimeAvailability;
    private Location location;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Object getArrival() {
        return arrival;
    }

    public void setArrival(Object arrival) {
        this.arrival = arrival;
    }

    public Object getArrivalTimestamp() {
        return arrivalTimestamp;
    }

    public void setArrivalTimestamp(Object arrivalTimestamp) {
        this.arrivalTimestamp = arrivalTimestamp;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public Integer getDepartureTimestamp() {
        return departureTimestamp;
    }

    public void setDepartureTimestamp(Integer departureTimestamp) {
        this.departureTimestamp = departureTimestamp;
    }

    public Object getDelay() {
        return delay;
    }

    public void setDelay(Object delay) {
        this.delay = delay;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public Prognosis getPrognosis() {
        return prognosis;
    }

    public void setPrognosis(Prognosis prognosis) {
        this.prognosis = prognosis;
    }

    public Object getRealtimeAvailability() {
        return realtimeAvailability;
    }

    public void setRealtimeAvailability(Object realtimeAvailability) {
        this.realtimeAvailability = realtimeAvailability;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
