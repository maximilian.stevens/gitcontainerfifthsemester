
package classes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Example {

    private List<Connection> connections = null;
    private From from;
    private To to;
    private Stations stations;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public List<Connection> getConnections() {
        return connections;
    }

    public void setConnections(List<Connection> connections) {
        this.connections = connections;
    }

    @Override
    public String toString() {
        return "Example{" +
                "connections=" + connections +
                ", from=" + from +
                ", to=" + to +
                ", stations=" + stations +
                ", additionalProperties=" + additionalProperties +
                '}';
    }

    public From getFrom() {
        return from;
    }

    public void setFrom(From from) {
        this.from = from;
    }

    public To getTo() {
        return to;
    }

    public void setTo(To to) {
        this.to = to;
    }

    public Stations getStations() {
        return stations;
    }

    public void setStations(Stations stations) {
        this.stations = stations;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
