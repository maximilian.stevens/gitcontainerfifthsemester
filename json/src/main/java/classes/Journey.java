
package classes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Journey {

    private String name;
    private String category;
    private Object subcategory;
    private Object categoryCode;
    private String number;
    private String operator;
    private String to;
    private List<PassList> passList = null;

    @Override
    public String toString() {
        return "Journey{" +
                "name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", subcategory=" + subcategory +
                ", categoryCode=" + categoryCode +
                ", number='" + number + '\'' +
                ", operator='" + operator + '\'' +
                ", to='" + to + '\'' +
                ", passList=" + passList +
                ", capacity1st=" + capacity1st +
                ", capacity2nd=" + capacity2nd +
                ", additionalProperties=" + additionalProperties +
                '}';
    }

    private Object capacity1st;
    private Object capacity2nd;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Object getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(Object subcategory) {
        this.subcategory = subcategory;
    }

    public Object getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(Object categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public List<PassList> getPassList() {
        return passList;
    }

    public void setPassList(List<PassList> passList) {
        this.passList = passList;
    }

    public Object getCapacity1st() {
        return capacity1st;
    }

    public void setCapacity1st(Object capacity1st) {
        this.capacity1st = capacity1st;
    }

    public Object getCapacity2nd() {
        return capacity2nd;
    }

    public void setCapacity2nd(Object capacity2nd) {
        this.capacity2nd = capacity2nd;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
