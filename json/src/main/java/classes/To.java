
package classes;

import java.util.HashMap;
import java.util.Map;

public class To {

    private Station station;
    private String arrival;
    private Integer arrivalTimestamp;
    private Object departure;
    private Object departureTimestamp;
    private Object delay;
    private String platform;
    private Prognosis prognosis;
    private Object realtimeAvailability;
    private Location location;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public Integer getArrivalTimestamp() {
        return arrivalTimestamp;
    }

    public void setArrivalTimestamp(Integer arrivalTimestamp) {
        this.arrivalTimestamp = arrivalTimestamp;
    }

    public Object getDeparture() {
        return departure;
    }

    public void setDeparture(Object departure) {
        this.departure = departure;
    }

    public Object getDepartureTimestamp() {
        return departureTimestamp;
    }

    public void setDepartureTimestamp(Object departureTimestamp) {
        this.departureTimestamp = departureTimestamp;
    }

    public Object getDelay() {
        return delay;
    }

    public void setDelay(Object delay) {
        this.delay = delay;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    @Override
    public String toString() {
        return "To{" +
                "station=" + station +
                ", arrival='" + arrival + '\'' +
                ", arrivalTimestamp=" + arrivalTimestamp +
                ", departure=" + departure +
                ", departureTimestamp=" + departureTimestamp +
                ", delay=" + delay +
                ", platform='" + platform + '\'' +
                ", prognosis=" + prognosis +
                ", realtimeAvailability=" + realtimeAvailability +
                ", location=" + location +
                ", additionalProperties=" + additionalProperties +
                '}';
    }

    public Prognosis getPrognosis() {
        return prognosis;
    }

    public void setPrognosis(Prognosis prognosis) {
        this.prognosis = prognosis;
    }

    public Object getRealtimeAvailability() {
        return realtimeAvailability;
    }

    public void setRealtimeAvailability(Object realtimeAvailability) {
        this.realtimeAvailability = realtimeAvailability;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
