import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;

import java.io.File;

public class Main {
    public static void main(String[] args) {

        try {

            // 1, 2, 3
            String jsonStr = FileUtils.readFileToString(new File("family.json"), "utf-8");

            Gson gson = new Gson();
            Family family = gson.fromJson(jsonStr, Family.class);
            family.getMembers().forEach(System.out::println);

            // 4. a
            String jsonStr2 = FileUtils.readFileToString(new File("connections.json"), "utf-8");
            Connections connections = gson.fromJson(jsonStr2, Connections.class);
            connections.getConnections().stream().limit(2).forEach(System.out::println);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}