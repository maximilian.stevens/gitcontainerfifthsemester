import classes.Connection;

import java.util.ArrayList;

public class Connections {

    private ArrayList<Connection> connections = new ArrayList<Connection>();

    public ArrayList<Connection> getConnections() {
        return connections;
    }

    public void setConnections(ArrayList<Connection> connections) {
        this.connections = connections;
    }

    public Connections(ArrayList<Connection> connections) {
        this.connections = connections;
    }
}
