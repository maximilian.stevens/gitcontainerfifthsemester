import java.util.ArrayList;

public class Family {

    private ArrayList<Person> members = new ArrayList<Person>();

    public ArrayList<Person> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<Person> members) {
        this.members = members;
    }

    public Family(ArrayList<Person> members) {
        this.members = members;
    }
}
