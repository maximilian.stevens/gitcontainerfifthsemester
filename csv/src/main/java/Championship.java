
import java.time.Year;
import java.util.Date;
public class Championship {

    private int id;
    private int calendarIDFS;
    private Year year;
    private Date datefrom;
    private Date dateto;
    private String championship;
    private String city;
    private String nation;
    private Date compdate;
    private Date comptimefrom;
    private Date comptimeto;
    private String competition;

    @Override
    public String toString() {
        return "Competition{" +
                "id=" + id +
                ", calendarIDFS=" + calendarIDFS +
                ", year='" + year + '\'' +
                ", datefrom='" + datefrom + '\'' +
                ", dateto='" + dateto + '\'' +
                ", championship='" + championship + '\'' +
                ", city='" + city + '\'' +
                ", nation='" + nation + '\'' +
                ", compdate='" + compdate + '\'' +
                ", comptimefrom='" + comptimefrom + '\'' +
                ", comptimeto='" + comptimeto + '\'' +
                ", competition='" + competition + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCalendarIDFS() {
        return calendarIDFS;
    }

    public void setCalendarIDFS(int calendarIDFS) {
        this.calendarIDFS = calendarIDFS;
    }

    public Year getYear() {
        return year;
    }

    public void setYear(Year year) {
        this.year = year;
    }

    public Date getDatefrom() {
        return datefrom;
    }

    public void setDatefrom(Date datefrom) {
        this.datefrom = datefrom;
    }

    public Date getDateto() {
        return dateto;
    }

    public void setDateto(Date dateto) {
        this.dateto = dateto;
    }

    public String getChampionship() {
        return championship;
    }

    public void setChampionship(String championship) {
        this.championship = championship;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public Date getCompdate() {
        return compdate;
    }

    public void setCompdate(Date compdate) {
        this.compdate = compdate;
    }

    public Date getComptimefrom() {
        return comptimefrom;
    }

    public void setComptimefrom(Date comptimefrom) {
        this.comptimefrom = comptimefrom;
    }

    public Date getComptimeto() {
        return comptimeto;
    }

    public Championship() {
    }

    public Championship(int id, int calendarIDFS, Year year, Date datefrom, Date dateto, String championship, String city, String nation, Date compdate, String competition) {
        this.id = id;
        this.calendarIDFS = calendarIDFS;
        this.year = year;
        this.datefrom = datefrom;
        this.dateto = dateto;
        this.championship = championship;
        this.city = city;
        this.nation = nation;
        this.compdate = compdate;
        this.competition = competition;
    }

    public void setComptimeto(Date comptimeto) {
        this.comptimeto = comptimeto;
    }

    public String getCompetition() {
        return competition;
    }

    public void setCompetition(String competition) {
        this.competition = competition;
    }
}
