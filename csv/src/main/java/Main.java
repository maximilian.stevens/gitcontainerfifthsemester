import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.ArrayList;


public class Main {

    public static void main(String[] args) {
        String line;
        ArrayList<Championship> championships = new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy");
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("hh:mm:ss");

        try {
            BufferedReader br = new BufferedReader(new FileReader("realtime.csv"));

            line = br.readLine();

            while ((line = br.readLine()) != null) {

                String[] data = line.split(";");

                Championship championship = new Championship();
                for (int i = 0; i < data.length; i++) {
                    data[i] = data[i].replaceAll("\"", "");
                    if (i == 9) {
                        if (!data[9].equals(" ")) {
                            championship.setComptimefrom(simpleTimeFormat.parse(data[i]));
                        } else {
                            championship.setComptimefrom(null);
                        }
                    } else if (i == 10) {
                        if (!data[10].equals(" ")) {
                            championship.setComptimefrom(simpleTimeFormat.parse(data[i]));
                        } else {
                            championship.setComptimeto(null);
                        }
                    }
                }

                championship = new Championship(
                        Integer.parseInt(data[0]),
                        Integer.parseInt(data[1]),
                        Year.parse(data[2]),
                        simpleDateFormat.parse(data[3]),
                        simpleDateFormat.parse(data[4]),
                        data[5],
                        data[6],
                        data[7],
                        simpleDateFormat.parse(data[8]),
                        data[11]);

                championships.add(championship);
            }

            championships.forEach(competition -> {
                if (competition.getYear().toString().equals("2013")) {
                    System.out.println("true");
                }
            });
//            championships.sort(Comparator.comparing(Competition::getCompdate));
//            championships.forEach(System.out::println);

            br.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
