import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class Main {

    Document document;

    public Main(String fileName) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(fileName);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void ersteZweiEbenenAusgeben() {
        Node rootNode = document.getDocumentElement();
        NodeList nodeList = rootNode.getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            print(node, "  --  ");

        }
    }

    public void alleKnotenRekursivAusgeben() {
    }

    private void print(Node node, String indent){
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            System.out.println(indent + node.getNodeName());

            NodeList list = node.getChildNodes();
            for (int i = 0; i < list.getLength(); i++) {
                Node subNode = list.item(i);
                print(subNode, indent);
            }
        }
    }

    public static void main(String args[]) {

        Main reader = new Main("issf-calendar-start.xml");


        reader.ersteZweiEbenenAusgeben();
        //reader.alleKnotenRekursivAusgeben();

    }

}